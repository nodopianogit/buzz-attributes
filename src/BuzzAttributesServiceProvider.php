<?php

namespace Nodopiano\Buzz\Attributes;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class BuzzAttributesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Nodopiano\Buzz\Attributes\Repositories\AttributeRepository', 'Nodopiano\Buzz\Attributes\Repositories\EloquentAttributeRepository');
    }
}
