<?php

namespace Nodopiano\Buzz\Attributes\Repositories;

use Nodopiano\Buzz\Attributes\Models\Attribute;

class EloquentAttributeRepository implements AttributeRepository
{
	private $model;

	function __construct(Attribute $model)
	{
		$this->model = $model;
	}

	public function save($attributes, $attributable)
	{
		$toUpdate = array_diff(array_keys($attributes), $attributable->getModelAttributes());

		foreach ($toUpdate as $key) {
			if(!empty($attributes[$key]))
				$attributable->saveAttribute($key, $attributes[$key]);
			else $attributable->deleteAttribute($key);
		}


	}

	public function delete($attributable)
	{
		$attributable->attributes()->delete();
	}
}