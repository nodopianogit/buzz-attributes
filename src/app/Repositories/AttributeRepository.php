<?php
namespace Nodopiano\Buzz\Attributes\Repositories;

interface AttributeRepository
{
    public function save($attributes, $attributable);
    public function delete($attributable);
}
