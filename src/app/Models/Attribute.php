<?php

namespace Nodopiano\Buzz\Attributes\Models;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $table = 'attributes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'key', 'value', 'attributable_id', 'attributable_type'
    ];

    public function attributable()
    {
        return $this->morphTo();
    }

    public function scopeSelect($query, $key)
    {
        return $query->where('key', $key);
    }

    public function scopeOf($query, $type)
    {
        return $query->where('attributable_type', $type);
    }

    public function scopeThatIs($query, $type_id)
    {
        return $query->where('attributable_id', $type_id);
    }

    public function scopeThatIsNot($query, $type_id)
    {
        return $query->where('attributable_id', '<>', $type_id);
    }

    public function scopeThatIsIn($query, $type_ids)
    {
        return $query->whereIn('attributable_id', $type_id);
    }

    public function scopeThatIsNotIn($query, $type_ids)
    {
        return $query->whereNotIn('attributable_id', $type_id);
    }

    public function scopeWithValue($query, $value)
    {
        return $query->where('value', $value);
    }

    public function scopeWithoutValue($query, $value)
    {
        return $query->where('value', '<>', $value);
    }

    public function scopeValueIn($query, $values)
    {
        return $query->whereIn('value', $values);
    }

    public function scopeValueNotIn($query, $values)
    {
        return $query->whereNotIn('value', $values);
    }

    public function scopeGetValue($query)
    {
        return ($query->get()->isNotEmpty())? $query->first()->value : null;
    }
}
