<?php

namespace Nodopiano\Buzz\Attributes\Traits;
use Nodopiano\Buzz\Attributes\Models\Attribute;

trait HasAttributes
{
    public function attributes()
    {
        return $this->morphMany('Nodopiano\Buzz\Attributes\Models\Attribute', 'attributable');
    }

    public static function hasAttributes()
    {
        return true;
    }

    public static function filter($key, $value)
    {
        return Attribute::select($key)->of(get_called_class())->withValue($value)->with('attributable')->get()->pluck('attributable');
    }

    public function getHasAttributesAttribute()
    {
        return true;
    }

    public function getAttributesAttribute()
    {
        return $this->attributes()->pluck('value', 'key');
    }

    public function getModelAttributes()
    {
        return array_keys($this->getAttributes());
    }

    public function getValueOf($key)
    {
        $query = $this->attributes()->select($key);

        return ($query->count() > 0) ? $query->getValue() : null;
    }

    public function saveAttribute($key, $value)
    {
        if($this->getValueOf($key) != null) {
            $attribute = $this->attributes()->select($key)->first();
            $attribute->update(['value' => $value]);
        }
        else {
            $this->attributes()->create([
                'key' => $key,
                'value' => $value
            ]);
        }
    }

    public function deleteAttribute($key)
    {
        if($this->getValueOf($key) != null) {
            $this->attributes()->select($key)->delete();
        }
    }
}